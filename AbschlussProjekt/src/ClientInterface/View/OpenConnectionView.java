package ClientInterface.View;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import sun.net.www.ApplicationLaunchException;

import java.io.IOException;

/**
 * Created by Gabriel Meier on 05.12.2016.
 */
public class OpenConnectionView extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        stage =
                FXMLLoader.load(
                        getClass()
                                .getResource("OpenConnection.fxml"));

        stage.show();
    }

    public void openConnection(){
        launch();
    }
}
